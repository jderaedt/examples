package com.experian.health.java;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by william.ott on 4/13/2016.
 */
public class Trades_WilliamTest {
    public final String TRADE_FILENAME = "tradedata.csv";
    TradesWilliam trades;

    @Before
    public void setUp() throws Exception{
        File file = new File(this.getClass().getResource(TRADE_FILENAME).getFile());
        trades = new TradesWilliam();
        trades.loadTrades(file.getAbsolutePath());

    }

    @Test
    public void validateFile()
    {
        BufferedReader reader = null;
        File file = new File(this.getClass().getResource(TRADE_FILENAME).getFile());
        try {
            reader = Files.newBufferedReader(Paths.get(file.getAbsolutePath()));

            String s = reader.readLine();

            //Check for the correct header.
            assert(s.equals("ID,TradeDate,BuySell,Currency1,Amount1,Exchange Rate,Currency2,Amount2,Settlement Date"));

        }
        catch(FileNotFoundException f)
        {

        }
        catch(IOException e)
        {

        }
    }

    @Test
    public void validateTradesHasRows()
    {
        assert(trades.getTrades().size()==10);
    }
}
