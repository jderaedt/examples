package com.experian.health.java;

import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;

import static org.junit.Assert.assertEquals;

/**
 * example test to demonstrate potential indirect consequences when changing code.
 */
public class PainTest {

    @Ignore
    @Test
    public void testFileCount() throws Exception {

        File folder = new File("./src/main/java");

        long count = Files.walk(folder.toPath())
                .filter(path -> !Files.isDirectory(path))
                .filter(path -> !path.getFileName().endsWith("*.java"))
                .count();

        assertEquals("number of files mismatch", count, count);
    }
}
