package com.experian.health.java;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class MichaelTradeTest {

    @Before
    public void InitializeTest()
    {
        /*

                                        */
    }

    @Test
    public void ConstructorTest()
    {
        MichaelTrade michaelTrade = new MichaelTrade();

        michaelTrade.setID(10);
        michaelTrade.setTradeDate(LocalDate.now().minusDays(2));
        michaelTrade.setBuySell("Buy");
        michaelTrade.setCurrency1("GBP");
        michaelTrade.setAmount1(new BigDecimal(12400000.00));
        michaelTrade.setExchangeRate(new BigDecimal(1.25846));
        michaelTrade.setCurrency2("USD");
        michaelTrade.setAmount2(new BigDecimal(15604904.00));
        michaelTrade.setSettlementDate(LocalDate.now());

        assert michaelTrade.equals(new MichaelTrade(michaelTrade.getID(),
                                                    michaelTrade.getTradeDate(),
                                                    michaelTrade.getBuySell(),
                                                    michaelTrade.getCurrency1(),
                                                    michaelTrade.getAmount1(),
                                                    michaelTrade.getExchangeRate(),
                                                    michaelTrade.getCurrency2(),
                                                    michaelTrade.getAmount2(),
                                                    michaelTrade.getSettlementDate()));

        assert ValidateMichaelTrade(michaelTrade);
    }

    @Test
    public void FileTest()
    {
        String fileLine;
        MichaelTrade fileMichaelTrade;
        Map<Integer, String> tradeIndexes = null;
        Map<Integer, MichaelTrade> michaelTrades = new HashMap<Integer, MichaelTrade>();

        try {
            // use a try like c# using so buffered reader will be disposed when finished
            try (FileReader reader = new FileReader(getClass().getResource("tradedata.csv").getPath()))
            {
                // use a try like c# using so buffered reader will be disposed when finished
                try (BufferedReader bufferedReader = new BufferedReader(reader)) {
                    fileLine = bufferedReader.readLine();
                    while(fileLine != null) {

                        fileMichaelTrade = new MichaelTrade();

                        // if we dont have trade indexes, this is the header line
                        if (tradeIndexes == null) {
                            tradeIndexes = fileMichaelTrade.parseTradeIndexes(fileLine);
                        }
                        else {
                            fileMichaelTrade.parseTrade(fileLine, tradeIndexes);
                            michaelTrades.put(fileMichaelTrade.getID(), fileMichaelTrade);
                        }

                        // read the next line
                        fileLine = bufferedReader.readLine();
                    }
                }
            }

            for (MichaelTrade michaelTrade : michaelTrades.values()) {
                assert ValidateMichaelTrade(michaelTrade);
            }

        } catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }

        assert michaelTrades.size() == 10;
    }

    private boolean ValidateMichaelTrade(MichaelTrade michaelTrade)
    {
        boolean validTrade = true;
        MathContext mathContext = new MathContext(6);

        validTrade = validTrade && (michaelTrade.getAmount1().multiply(michaelTrade.getExchangeRate().round(mathContext)).compareTo(michaelTrade.getAmount2()) == 0);
        validTrade = validTrade && (michaelTrade.getTradeDate().isBefore(michaelTrade.getSettlementDate()));
        validTrade = validTrade && (michaelTrade.getBuySell().equals("Buy") || michaelTrade.getBuySell().equals("Sell"));


        return validTrade;
    }
}
