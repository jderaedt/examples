package com.experian.health.java;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by joe.khym on 4/13/2016.
 */
public class JoeTradeTest {
    public final String TRADEDATA_FILENAME = "tradedata.csv";
    private List<JoeTrade> joeTrades = new ArrayList();
    private JoeTrade joeTradeTest;

    @Before
    public void setup() {
        //This should be a method is a future class because it will be re-used
        InputStream inputStream = JoeTradeTest.class.getResourceAsStream(TRADEDATA_FILENAME);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        reader.lines().skip(1).forEach(line -> {
                JoeTrade joeTrade = new JoeTrade();
                joeTrade.loadByParse(line);
                joeTrades.add(joeTrade);
        }
        );
    }

    @Test
    public void print1000Trades(){
        int size = joeTrades.size();
        if (size > 1000) {
            size = 1000;
        }
        joeTrades.subList(1, size).forEach(j -> System.out.println(j.toString()));
    }


}
