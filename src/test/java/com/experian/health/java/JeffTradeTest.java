package com.experian.health.java;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.time.DayOfWeek;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;

public class JeffTradeTest {

    private static final String TRADE_DATA_FILENAME = "/com/experian/health/java/tradedata.csv";
    private static List<JeffTrade> trades;

    @BeforeClass
    public static void loadTradeDataFile() throws IOException {
        long start = System.nanoTime();

        File file = new File(JeffTradeTest.class.getResource(TRADE_DATA_FILENAME).getFile());
        try (Stream<String> lines = Files.lines(Paths.get(file.toURI()))) {
            trades = lines.skip(1)
                             .parallel()
                             .map(line -> {
                                 try {
                                     return JeffTrade.parseCsvLine(line);
                                 } catch (ParseException e) {
                                     throw new RuntimeException(e);
                                 }
                             })
                             .collect(Collectors.toList());
        }

        double duration = (System.nanoTime() - start) / 1e9;
        System.out.println("trades = " + trades.size());
        System.out.println("duration = " + duration);
    }

    @Test
    public void ensureCurrenciesAreDifferent() {
        boolean actual = trades.stream()
                                 .allMatch(trade -> !trade.getSourceCurrency().equals(trade.getTargetCurrency()));
        assertTrue(actual);
    }

    @Test
    public void ensureSettlementDateIsAfterTheTradeDate() {
        boolean actual = trades.stream()
                                 .allMatch(trade -> trade.getTradeDate().before(trade.getSettlementDate()));
        assertTrue(actual);
    }

    @Test
    public void ensureSettlementDateIsAWeekDay() {
        boolean actual = trades.stream()
                                 .allMatch(trade -> {
                                     DayOfWeek dow = trade.getSettlementDate()
                                                             .toInstant()
                                                             .atZone(ZoneId.of("GMT"))
                                                             .getDayOfWeek();
                                     return dow != DayOfWeek.SATURDAY && dow != DayOfWeek.SUNDAY;
                                 });
        assertTrue(actual);
    }

    @Test
    public void ensureSettlementValuesAreCorrect() {
        boolean actual = trades.stream()
                                 .allMatch(trade -> trade.getSourceAmount()
                                                            .multiply(BigDecimal.valueOf(trade.getExchangeRate()))
                                                            .compareTo(trade.getTargetAmount()) == 0);
        assertTrue(actual);
    }

    @Test
    public void ensureTradeActionIsValid() {
        boolean actual = trades.stream()
                                 .allMatch(trade -> {
                                     String tradeAction = trade.getTradeAction();
                                     return tradeAction.equalsIgnoreCase("buy") || tradeAction.equalsIgnoreCase("sell");
                                 });
        assertTrue(actual);
    }
}
