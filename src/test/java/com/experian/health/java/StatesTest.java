package com.experian.health.java;

import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by joe.khym on 4/12/2016.
 */
public class StatesTest {

    public final String STATE_FILENAME = "StateList.csv";
    private StateUtils states;

    @Before
    public void setup() {
        //This converts the windows path into a valid pathname. I got an error if I didn't do this.
        File file = new File(this.getClass().getResource(STATE_FILENAME).getFile());
        states = new StateUtils(file.getAbsolutePath());
    }

    @Test
    public void testValidState() throws Exception {
        assertTrue(states.isValid(new State("Tennessee", "TN")));
    }

    @Test
    public void testMissingState() throws Exception {
        assertFalse(states.isValid(new State("MonkeyLand", "MK")));
    }

    @Test
    public void testValidStateAbbreviationCaseUpper() throws Exception {
        assertTrue(states.isValidStateAbbreviation("TN"));
    }

    @Test
    public void testValidStateAbbreviationCaseLower() throws Exception {
        assertTrue(states.isValidStateAbbreviation("tn"));
    }

    @Test
    public void testMissingStateAbbreviation() throws Exception {
        assertFalse(states.isValidStateAbbreviation("ZZ"));
    }

    @Test
    public void testValidStateNameCaseUpper() throws Exception {
        assertTrue(states.isValidStateName("TENNESSEE"));
    }

    @Test
    public void testValidStateNameCaseLower() throws Exception {
        assertTrue(states.isValidStateName("tennessee"));
    }

    @Test
    public void testMissingStateName() throws Exception {
        assertFalse(states.isValidStateName("Zebra"));
    }
    @Test
    public void testGetStateByStateName() throws Exception {
        State tnState = new State("Tennessee", "TN");
        State testState = states.getStateByStateName("Tennessee");
        assertTrue(tnState.equals(testState));
    }
    @Test
    public void testGetStateByStateNameMissing() throws Exception {
        State tnState = new State("Tennessee", "TN");
        State testState = states.getStateByStateName("Zebra");
        assertFalse(tnState.equals(testState));
    }
    @Test
    public void testGetStateByStateAbbreviation() throws Exception {
        State tnState = new State ("Tennessee","TN");
        State testState = states.getStateByStateAbbreviation("TN");
        assertTrue(tnState.equals(testState));
    }
    @Test
    public void testGetStateByStateAbbreviationMissing() throws Exception {
        State tnState = new State ("Tennessee","TN");
        State testState = states.getStateByStateAbbreviation("ZZ");
        assertFalse(tnState.equals(testState));
    }

}