package com.experian.health.java;

import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by keith.wenzler on 4/13/2016.
 */
public class KeithTradeTest {

    private KeithTrade keithTrade;

    @Test
    public void testLoadKeithTrade() throws Exception {

        InputStream inputStream = null;
        Reader reader = null;
        BufferedReader bufferedReader = null;
        String trade = null;

        try {

            inputStream = KeithTradeTest.class.getResourceAsStream("tradedata.csv");

            reader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(reader);

            bufferedReader.readLine();

            while ((trade = bufferedReader.readLine()) != null) {

                keithTrade = new KeithTrade(trade);

                assertEquals("trades should match", keithTrade.toString(), trade);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
