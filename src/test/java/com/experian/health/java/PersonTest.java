package com.experian.health.java;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 */
public class PersonTest {

    private Person person;

    @Before
    public void setUp() throws Exception {
        person = new Person("Dave", LocalDate.of(2000, 10, 10));
    }

    @Test
    public void testGetName() throws Exception {


        assertEquals("names should match", person.getName(), "Dave");
    }

    @Test
    public void test10kPeople() {
        Random random = new Random();
        long start = System.nanoTime();
        for (int i=0; i <1_000_000; i++) {
            new Person("Name" + i, LocalDate.of(random.nextInt(50 + 1950),
                    1+ random.nextInt(11), 1+ random.nextInt(27)));
        }

        double duration = (System.nanoTime() - start) / 1e9;
        System.out.println("duration = " + duration);
        assertThat("duration to long", duration, lessThan(5.0));
    }

    @Test
    public void test10kPeopleAddedToList() {
        Random random = new Random();
        List<Person> persons = new ArrayList();
        long start = System.nanoTime();
        for (int i=0; i <1_000_000; i++) {
            persons.add(new Person("Name" + i, LocalDate.of(random.nextInt(50 + 1950),
                    1+ random.nextInt(11), 1+ random.nextInt(27))));

        }

        double duration = (System.nanoTime() - start) / 1e9;
        System.out.println("list add duration = " + duration);
        assertThat("duration to long", duration, lessThan(5.0));
    }

    @Test
    public void test10kPeopleAddedToListPreAllocated() {
        Random random = new Random();
        List<Person> persons = new ArrayList(1_000_000);
        long start = System.nanoTime();
        for (int i=0; i <1_000_000; i++) {
            persons.add(new Person("Name" + i, LocalDate.of(random.nextInt(50 + 1950),
                    1+ random.nextInt(11), 1+ random.nextInt(27))));

        }

        double duration = (System.nanoTime() - start) / 1e9;
        System.out.println("pre allocated list add duration = " + duration);
        assertThat("duration to long", duration, lessThan(5.0));
    }

    @Test
    public void test10kPeopleAddedToListLambda() {
        Random random = new Random();
        List<Person> persons = new ArrayList(1_000_000);
        long start = System.nanoTime();
        IntStream.iterate(0, n->n+1).limit(1_000_000)
        .forEach (i ->
            persons.add(new Person("Name" + i, LocalDate.of(random.nextInt(50 + 1950),
                    1+ random.nextInt(11), 1+ random.nextInt(27)))));


        double duration = (System.nanoTime() - start) / 1e9;
        System.out.println("lambda pre alloc list add duration = " + duration);
        assertThat("duration to iteration", duration, lessThan(10.0));
    }



    @Test
    public void test10PersonSort() {

        Random random = new Random();
        List<Person> persons = new ArrayList();
        IntStream.iterate(0, n->n+1).limit(10)
                .forEach (i ->
                        persons.add(new Person("Name" + i, LocalDate.of(random.nextInt(50 + 1950),
                                1+ random.nextInt(11), 1+ random.nextInt(27)))));


//        persons.sort(Person::compareByAge);

        persons.forEach(System.out::println);



        try {
              final FileWriter fw = new FileWriter(new File("out.txt"));
            persons.forEach(p -> writeToFile(p.toString(), fw));
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }

    }

    @Test
    public void testAddress() throws Exception {
        person.setAddress("123 Something Dr");
        assertEquals("addresses should match", person.getAddress(), "123 Something Dr");
    }

    private void writeToFile(String person, FileWriter fw) {

        try {
            fw.write(person + "\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    public void canICuss()
    {
        assert person.doCuss() == "Shut the front door!";
    }


}