package net.projecteuler.experianhealth;

import org.junit.Test;

/**
 * Created by jdavies on 14/04/2016.
 */
public class Euler4WilliamTest {
    private static final String SUB_PACKAGE = "william";

/*
    @Test( timeout = 5000 )
    public void testSolution7() {
        Solution solution = getSolutionForProblem(7);
        assert solution.answer() == 104743;
    }

    @Test( timeout = 5000 )
    public void testSolution8() {
        Solution solution = getSolutionForProblem(8);
        assert solution.answer() == 23514624000L;
    }

    @Test( timeout = 5000 )
    public void testSolution9() {
        Solution solution = getSolutionForProblem(9);
        assert solution.answer() == 31875000;
    }

    @Test( timeout = 5000 )
    public void testSolution10() {
        Solution solution = getSolutionForProblem(10);
        assert solution.answer() == 142913828922L;
    }

    @Test( timeout = 5000 )
    public void testSolution11() {
        Solution solution = getSolutionForProblem(11);
        assert solution.answer() == 70600674;
    }

    @Test( timeout = 5000 )
    public void testSolution12() {
        Solution solution = getSolutionForProblem(12);
        assert solution.answer() == 76576500;
    }

    @Test( timeout = 5000 )
    public void testSolution14() {
        Solution solution = getSolutionForProblem(14);
        assert solution.answer() == 837799;
    }

    @Test( timeout = 5000 )
    public void testSolution16() {
        Solution solution = getSolutionForProblem(16);
        assert solution.answer() == 1366;
    }

    @Test( timeout = 5000 )
    public void testSolution25() {
        Solution solution = getSolutionForProblem(25);
        assert solution.answer() == 4782;
    }
*/
    public static Solution getSolutionForProblem(int prob) {
        Solution solution = null;

        try {
            Class<Solution> problem1 = (Class<Solution>) Class.forName("net.projecteuler.experianhealth."+SUB_PACKAGE+".Problem" + prob);
            solution = problem1.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            // do nothing, nothing valid found
        }
        return solution;
    }

    /*
    @Test( timeout = 5000 )
    public void testSolution2() {
        Solution solution = getSolutionForProblem(2);
        assert solution.answer() == 4613732;
    }
    */

    @Test(timeout = 5000)
    public void testSolution1() {
        Solution solution = getSolutionForProblem(1);
        assert solution.answer() == 233168;
    }

    @Test(timeout = 5000)
    public void testSolution3() {
        Solution solution = getSolutionForProblem(3);
        assert solution.answer() == 6857;
    }

    @Test(timeout = 5000)
    public void testSolution4() {
        Solution solution = getSolutionForProblem(4);
        assert solution.answer() == 906609;
    }

    @Test(timeout = 25000)
    public void testSolution5() {
        Solution solution = getSolutionForProblem(5);
        assert solution.answer() == 232792560;
    }

    @Test(timeout = 5000)
    public void testSolution6() {
        Solution solution = getSolutionForProblem(6);
        assert solution.answer() == 25164150;
    }
}

