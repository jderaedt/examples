package net.projecteuler.experianhealth.joe;

import net.projecteuler.experianhealth.Solution;
import org.junit.Test;

/**
 * Created by jdavies on 14/04/2016.
 */
public class Euler4Test {
    private static final String SUB_PACKAGE = "joe";

    @Test( timeout = 1000 )
    public void testSolution1() {
        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(1);
        assert solution.answer() == 233168;
    }

    @Test( timeout = 1000 )
    public void testSolution2() {
        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(2);
        assert solution.answer() == 4613732;
    }

    @Test( timeout = 1000 )
    public void testSolution3() {
        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(3);
        assert solution.answer() == 6857;
    }

    @Test( timeout = 1000 )
    public void testSolution4() {
        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(4);
        assert solution.answer() == 906609;
    }

    @Test( timeout = 1000 )
    public void testSolution5() {
        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(5);
        assert solution.answer() == 232792560;
    }

    @Test( timeout = 1000 )
    public void testSolution6() {
        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(6);
        assert solution.answer() == 25164150;
    }

    @Test( timeout = 1000 )
    public void testSolution7() {
        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(7);
        assert solution.answer() == 104743;
    }

    @Test( timeout = 1000 )
    public void testSolution8() {
        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(8);
        assert solution.answer() == 23514624000L;
    }

//    @Test( timeout = 20 )
//    public void testSolution9() {
//        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(9);
//        assert solution.answer() == 31875000;
//    }
//
//    @Test( timeout = 2000 )
//    public void testSolution10() {
//        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(10);
//        assert solution.answer() == 142913828922L;
//    }
//
//    @Test( timeout = 10 )
//    public void testSolution11() {
//        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(11);
//        assert solution.answer() == 70600674;
//    }
//
//    @Test( timeout = 3000 )
//    public void testSolution12() {
//        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(12);
//        assert solution.answer() == 76576500;
//    }
//
//    @Test( timeout = 50 )
//    public void testSolution13() {
//        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(13);
//        assert solution.answer() == 5537376230L;
//    }
//
//    @Test( timeout = 3000 )
//    public void testSolution14() {
//        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(14);
//        assert solution.answer() == 837799;
//    }
//
//    @Test( timeout = 10 )
//    public void testSolution15() {
//        net.projecteuler.experianhealth.Solution solution = getSolutionForProblem(15);
//        assert solution.answer() == 137846528820L;
//    }

    public static net.projecteuler.experianhealth.Solution getSolutionForProblem(int prob) {
        net.projecteuler.experianhealth.Solution solution = null;

        try {
            Class<net.projecteuler.experianhealth.Solution> problem1 = (Class<net.projecteuler.experianhealth.Solution>) Class.forName("net.projecteuler.experianhealth."+SUB_PACKAGE+".Problem" + prob);
            solution = problem1.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            // do nothing, nothing valid found
        }
        return solution;
    }
}

