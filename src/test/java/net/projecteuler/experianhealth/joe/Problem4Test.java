package net.projecteuler.experianhealth.joe;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by joe.khym on 4/13/2016.
 */
public class Problem4Test {
    @Test
    public void testIsPalindromeTrue() throws Exception {
        assertTrue(Problem4.isPalindrome(900009L));
    }
    @Test
    public void testIsPalindromeFalse() throws Exception {
        assertFalse(Problem4.isPalindrome(900339L));
    }
}