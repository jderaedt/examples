package net.projecteuler.experianhealth;

import org.junit.Test;

/**
 * Created by michael.kirton on 4/14/2016.
 */
public class Euler4MichaelTest {

    private static final String SUB_PACKAGE = "michael";

    @Test( timeout = 100 )
    public void testSolution1() {
        Solution solution = getSolutionForProblem(1);
        assert solution.answer() == 233168;
    }

    @Test( timeout = 30 )
    public void testSolution2() {
        Solution solution = getSolutionForProblem(2);
        assert solution.answer() == 4613732;
    }

    @Test( timeout = 300 )
    public void testSolution3() {
        Solution solution = getSolutionForProblem(3);
        assert solution.answer() == 6857;
    }

    @Test( timeout = 1000 )
    public void testSolution4() {
        Solution solution = getSolutionForProblem(4);
        assert solution.answer() == 906609;
    }

    @Test( timeout = 30 )
    public void testSolution5() {
        Solution solution = getSolutionForProblem(5);
        assert solution.answer() == 232792560;
    }

    @Test( timeout = 50 )
    public void testSolution6() {
        Solution solution = getSolutionForProblem(6);
        assert solution.answer() == 25164150;
    }

    @Test( timeout = 1000 )
    public void testSolution7() {
        Solution solution = getSolutionForProblem(7);
        assert solution.answer() == 104743;
    }

    public static Solution getSolutionForProblem(int prob) {
        Solution solution = null;

        try {
            Class<Solution> problem1 = (Class<Solution>) Class.forName("net.projecteuler.experianhealth."+SUB_PACKAGE+".Problem" + prob);
            solution = problem1.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            // do nothing, nothing valid found
        }
        return solution;
    }
}
