package net.projecteuler.experianhealth;

import net.projecteuler.experianhealth.Solution;

/**
 * Created by jdavies on 14/04/2016.
 */
public class Answers {
    private static String subPackage = null;

    public static void main(String[] args) {
        if( args.length < 1 ) {
            System.out.println("Please pass in the sub-package for the test, e.g. Answers john");
            System.exit(0);
        }

        subPackage = args[0];

        for (int problem = 1; problem < 600; problem++) {
            Solution solution = getSolutionForProblem(problem);
            if (solution != null) {
                long answer = solution.answer();    // Run once first
                long start = System.nanoTime();
                answer = solution.answer();         // Now time it
                long duration = System.nanoTime() - start;

                if (duration < 1_000_000_000L) {
                    long tp10s = 10_000_000_000L / duration;
                    for (long count = 0; count < tp10s && count < 100_000; count++) {
                        start = System.nanoTime();
                        answer = solution.answer();
                    }
                    duration = System.nanoTime() - start;
                }
                if (duration < 1_000)
                    System.out.printf("Problem %d:\t%d\t(%d nS)%n", problem, answer, duration);
                else if (duration < 1_000_000)
                    System.out.printf("Problem %d:\t%d\t(%,.2f µS)%n", problem, answer, duration / 1e3);
                else if (duration < 1_000_000_000)
                    System.out.printf("Problem %d:\t%d\t(%,.2f mS)%n", problem, answer, duration / 1e6);
                else
                    System.out.printf("Problem %d:\t%d\t(%,.2f seconds)%n", problem, answer, duration / 1e9);
            }
        }
    }

    public static Solution getSolutionForProblem(int prob) {
        Solution solution = null;

        try {
            Class<Solution> problem1 = (Class<Solution>) Class.forName("net.projecteuler.experianhealth."+subPackage+".Problem" + prob);
            solution = problem1.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            // do nothing, nothing valid found
        }
        return solution;
    }
}
