package com.experian.health.java;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by william.ott on 4/13/2016.
 */
public class TradesWilliam {

    private List<TradeWilliam> trades;
    private FileLayout layout;

    public TradesWilliam() {

        trades = new ArrayList<>();
        layout = new FileLayout();
    }

    public void loadTrades(String fileName) {

        BufferedReader reader = null;

        try {
            reader = Files.newBufferedReader(Paths.get(fileName));

            String firstLine = reader.lines().findFirst().get();
            String[] header = firstLine.split(",");

            for (int i = 0; i < header.length; i++) {
                String s = header[i].toUpperCase().trim();
                switch (s) {
                    case "ID":
                        layout.setID(i);
                        break;
                    case "TRADEDATE":
                        layout.setTradeDate(i);
                        break;
                    case "BUYSELL":
                        layout.setType(i);
                        break;
                    case "CURRENCY1":
                        layout.setCurrency1(i);
                        break;
                    case "AMOUNT1":
                        layout.setAmount1(i);
                        break;
                    case "EXCHANGE RATE":
                        layout.setRate(i);
                        break;
                    case "CURRENCY2":
                        layout.setCurrency2(i);
                        break;
                    case "AMOUNT2":
                        layout.setAmount2(i);
                        break;
                    case "SETTLEMENT DATE":
                        layout.setSettlementDate(i);
                        break;
                    default:
                        System.out.printf("ERROR: %s%n", s);
                        break;

                }
            }

            reader.lines().forEach(line -> {
                String[] data = line.split(",");


                if (isNumeric(data[layout.getID()])) {
                    TradeWilliam trade = new TradeWilliam();

                    trade.setID(Integer.parseInt(data[layout.getID()]));
                    trade.setTradeDate(convertStringToDateTime(data[layout.getTradeDate()]));
                    trade.setTradeType(data[layout.getType()]);
                    trade.setCurrency1(data[layout.getCurrency1()]);
                    trade.setAmount1(new BigDecimal(data[layout.getAmount1()].replace(",", "")));
                    trade.setExchangeRates(new BigDecimal(data[layout.getRate()].replace(",", "")));
                    trade.setCurrency2(data[layout.getCurrency2()]);
                    trade.setAmount2(new BigDecimal(data[layout.getAmount2()].replace(",", "")));
                    trade.setSettlementDate(convertStringToDateTime(data[layout.getSettlementDate()]));

                    trades.add(trade);

                }
            });
        } catch (FileNotFoundException f) {
            f.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<TradeWilliam> getTrades() {
        return trades;
    }

    private LocalDate convertStringToDateTime(String data) {
        return LocalDate.parse(data, DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH));
    }

    private static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private class FileLayout {
        private int ID;
        private int TradeDate;
        private int Type;
        private int Currency1;
        private int Amount1;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public int getTradeDate() {
            return TradeDate;
        }

        public void setTradeDate(int tradeDate) {
            TradeDate = tradeDate;
        }

        public int getType() {
            return Type;
        }

        public void setType(int type) {
            Type = type;
        }

        public int getCurrency1() {
            return Currency1;
        }

        public void setCurrency1(int currency1) {
            Currency1 = currency1;
        }

        public int getAmount1() {
            return Amount1;
        }

        public void setAmount1(int amount1) {
            Amount1 = amount1;
        }


        public int getRate() {
            return Rate;
        }

        public void setRate(int rate) {
            Rate = rate;
        }

        public int getCurrency2() {
            return Currency2;
        }

        public void setCurrency2(int currency2) {
            Currency2 = currency2;
        }

        public int getAmount2() {
            return Amount2;
        }

        public void setAmount2(int amount2) {
            Amount2 = amount2;
        }

        public int getSettlementDate() {
            return SettlementDate;
        }

        public void setSettlementDate(int settlementDate) {
            SettlementDate = settlementDate;
        }


        private int Rate;
        private int Currency2;
        private int Amount2;
        private int SettlementDate;
    }
}
