package com.experian.health.java;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class MichaelTrade {

        /* Private variables */
    private int iD;
    private LocalDate tradeDate;
    private String buySell;
    private String currency1;
    private BigDecimal amount1;
    private BigDecimal exchangeRate;
    private String currency2;
    private BigDecimal amount2;
    private LocalDate settlementDate;

    /* Constructors */
    public MichaelTrade(int iD,
                        LocalDate tradeDate,
                        String buySell,
                        String currency1,
                        BigDecimal amount1,
                        BigDecimal exchangeRate,
                        String currency2,
                        BigDecimal amount2,
                        LocalDate settlementDate)
    {
        this.iD = iD;
        this.tradeDate = tradeDate;
        this.buySell = buySell;
        this.currency1 = currency1;
        this.amount1 = amount1;
        this.exchangeRate = exchangeRate;
        this.currency2 = currency2;
        this.amount2 = amount2;
        this.settlementDate = settlementDate;
    }

    public MichaelTrade()
    {

    }

    @Override
    public String toString() {
        return "MichaelTrade{" +
                "iD=" + iD +
                ", tradeDate=" + tradeDate +
                ", buySell='" + buySell + '\'' +
                ", currency1='" + currency1 + '\'' +
                ", amount1=" + amount1 +
                ", exchangeRate=" + exchangeRate +
                ", currency2='" + currency2 + '\'' +
                ", amount2=" + amount2 +
                ", settlementDate=" + settlementDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MichaelTrade that = (MichaelTrade) o;

        if (iD != that.iD) return false;
        if (!tradeDate.equals(that.tradeDate)) return false;
        if (!buySell.equals(that.buySell)) return false;
        if (!currency1.equals(that.currency1)) return false;
        if (!amount1.equals(that.amount1)) return false;
        if (!exchangeRate.equals(that.exchangeRate)) return false;
        if (!currency2.equals(that.currency2)) return false;
        if (!amount2.equals(that.amount2)) return false;
        return settlementDate.equals(that.settlementDate);

    }

    @Override
    public int hashCode() {
        int result = iD;
        result = 31 * result + tradeDate.hashCode();
        result = 31 * result + buySell.hashCode();
        result = 31 * result + currency1.hashCode();
        result = 31 * result + amount1.hashCode();
        result = 31 * result + exchangeRate.hashCode();
        result = 31 * result + currency2.hashCode();
        result = 31 * result + amount2.hashCode();
        result = 31 * result + settlementDate.hashCode();
        return result;
    }
/* Getters/Setters */

    public int getID() {
        return iD;
    }

    public void setID(int iD) {
        this.iD = iD;
    }

    public LocalDate getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(LocalDate tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getBuySell() {
        return buySell;
    }

    public void setBuySell(String buySell) {
        this.buySell = buySell;
    }

    public String getCurrency1() {
        return currency1;
    }

    public void setCurrency1(String currency1) {
        this.currency1 = currency1;
    }

    public BigDecimal getAmount1() {
        return amount1;
    }

    public void setAmount1(BigDecimal amount1) {
        this.amount1 = amount1;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getCurrency2() {
        return currency2;
    }

    public void setCurrency2(String currency2) {
        this.currency2 = currency2;
    }

    public BigDecimal getAmount2() {
        return amount2;
    }

    public void setAmount2(BigDecimal amount2) {
        this.amount2 = amount2;
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(LocalDate settlementDate) {
        this.settlementDate = settlementDate;
    }

    /* public methods */
    public void parseTrade(String tradeFileLine, Map<Integer, String> tradeIndexes) throws Exception
    {
        final String[] tradeFileParts = tradeFileLine.split(",");
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        for (int index = 0; index < tradeFileParts.length; index++)
        {
            switch (tradeIndexes.get(index).toUpperCase()) {
                case "ID":
                    iD = Integer.parseInt(tradeFileParts[index]);
                    break;
                case "TRADEDATE":
                    tradeDate = LocalDate.parse(tradeFileParts[index], formatter);
                    break;
                case "BUYSELL":
                    buySell = tradeFileParts[index];
                    break;
                case "CURRENCY1":
                    currency1 = tradeFileParts[index];
                    break;
                case "AMOUNT1":
                    amount1 = new BigDecimal(tradeFileParts[index]);
                    break;
                case "EXCHANGE RATE":
                    exchangeRate = new BigDecimal(tradeFileParts[index]);
                    break;
                case "CURRENCY2":
                    currency2 = tradeFileParts[index];
                    break;
                case "AMOUNT2":
                    amount2 = new BigDecimal(tradeFileParts[index]);
                    break;
                case "SETTLEMENT DATE":
                    settlementDate = LocalDate.parse(tradeFileParts[index], formatter);
                    break;
                default:
                    throw new Exception(String.format("Unhandled Header In File: %s", tradeIndexes.get(index)));
            }
        }
    }

    public Map<Integer, String> parseTradeIndexes(String tradeFileHeaderLine)
    {
        Map<Integer, String> tradeIndexes = new HashMap<>();
        final String[] tradeFileHeaders = tradeFileHeaderLine.split(",");

        for (int index = 0; index < tradeFileHeaders.length; index++)
        {
            tradeIndexes.put(index, tradeFileHeaders[index]);
        }

        return tradeIndexes;
    }
}
