package com.experian.health.java;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by joe.khym on 4/12/2016.
 */
public class State {
    private String stateName;
    private String stateAbbreviation;

    public State(String stateName, String stateAbbreviation)
    {
        this.stateAbbreviation = stateAbbreviation.toUpperCase();
        this.stateName = stateName.toUpperCase();
    }

    public static boolean isValid( String abbreviation ) {
        return true;
    }

    public String getStateName() {
        return stateName;
    }

    public String getStateAbbreviation() {
        return stateAbbreviation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        State state = (State) o;

        if (stateName != null ? !stateName.equals(state.stateName) : state.stateName != null) return false;
        return stateAbbreviation != null ? stateAbbreviation.equals(state.stateAbbreviation) : state.stateAbbreviation == null;

    }

    @Override
    public int hashCode() {
        int result = stateName != null ? stateName.hashCode() : 0;
        result = 31 * result + (stateAbbreviation != null ? stateAbbreviation.hashCode() : 0);
        return result;
    }
}
    class StateUtils {
        private List<State> stateList;

        public StateUtils(String statesFileName)
        {
            this.stateList = loadStatesFile(statesFileName);
        }
        public boolean isValid(State state)
        {
            return stateList.contains(state);
        }
        public boolean isValidStateAbbreviation(String stateAbbreviation)
        {
            boolean result = false;
            for (State s : stateList)
            {
                if (s.getStateAbbreviation().equals(stateAbbreviation.toUpperCase()))
                {
                    result = true;
                    break;
                }
            }
            return result;
        };
        public boolean isValidStateName(String stateName)
        {
            boolean result = false;
            for (State s : stateList)
            {
                if (s.getStateName().equals(stateName.toUpperCase()))
                {
                    result = true;
                    break;
                }
            }
            return result;
        };
        public State getStateByStateName(String stateName)
        {
            State stateResult = null;
            for (State s : stateList)
            {
                if (s.getStateName().equals(stateName.toUpperCase()))
                {
                    stateResult = s;
                    break;
                }
            }
            return stateResult;
        }
        public State getStateByStateAbbreviation(String stateAbbreviation)
        {
            State stateResult = null;
            for (State s : stateList)
            {
                if (s.getStateAbbreviation().equals(stateAbbreviation.toUpperCase()))
                {
                    stateResult = s;
                    break;
                }
            }
            return stateResult;
        }
        private List<State> loadStatesFile(String statesFileName)
        {
            List<State> stateList = new ArrayList<>();
            BufferedReader reader = null;
            try {
                reader = Files.newBufferedReader(Paths.get(statesFileName));
            } catch (IOException e1) {
                e1.printStackTrace();
                return stateList;
            }

            reader.lines().forEach(line -> {
                String[] props = line.split(",");
                if(!props[0].contains("StateName"))
                {
                    stateList.add(new State(props[0], props[1].trim().toUpperCase()));
                }
            });

            try {
                reader.close();
            } catch (IOException e1) {
                e1.printStackTrace();
                return stateList;
            }

            return stateList;
        }
    }

