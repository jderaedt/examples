package com.experian.health.java;

/**
 */
public class NoCoverageHere {

    private String string;
    public NoCoverageHere(String string)
    {

                        this.string = string;
    }


    public void doStuff(){

        int i=0;
        for (int c=0; c< 100; c++){
              System.out.println(string + c + i);
        }
    }


    public void bigAndPointless(int i){

        if (i==1){
            System.out.println("1");
        }

        if (i==2){
            System.out.println("2");
        }
        if (i==3){
            System.out.println("3");
        } else if (i==4){
            System.out.println("4");
        }
        else if (i==5){
            System.out.println("5");
        }
        if (i==6){
            System.out.println("6");
        }
        if (i==7){
            System.out.println("7");
        }
        if (i==8){
            System.out.println("6");
        }
        else if (i==9){
            System.out.println("7");
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NoCoverageHere that = (NoCoverageHere) o;

        return !(string != null ? !string.equals(that.string) : that.string != null);

    }

    @Override
    public int hashCode() {
        return string != null ? string.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "NoCoverageHere{" +
                "string='" + string + '\'' +
                '}';
    }
}
