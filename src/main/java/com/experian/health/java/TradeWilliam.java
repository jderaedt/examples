package com.experian.health.java;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.Formatter;

/**
 * Created by william.ott on 4/13/2016.
 */
public class TradeWilliam {
    private int id;
    private LocalDate tradeDate;
    private String tradeType;
    private String currency1;
    private String currency2;
    private BigDecimal amount1;
    private BigDecimal amount2;
    private BigDecimal exchangeRates;
    private LocalDate settlementDate;

    public int getID() {
        return id;
    }
    public void setID(int ID) {
        this.id = ID;
    }

    public LocalDate getTradeDate() {
        return tradeDate;
    }
    public void setTradeDate(LocalDate tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getTradeType() {
        return tradeType;
    }
    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getCurrency1() {
        return currency1;
    }
    public void setCurrency1(String currency1) {
        this.currency1 = currency1;
    }

    public String getCurrency2() {
        return currency2;
    }
    public void setCurrency2(String currency2) {
        this.currency2 = currency2;
    }

    public BigDecimal getAmount1() {
        return amount1;
    }
    public void setAmount1(BigDecimal amount1) {
        this.amount1 = amount1;
    }

    public BigDecimal getAmount2() {
        return amount2;
    }
    public void setAmount2(BigDecimal amount2) {
        this.amount2 = amount2;
    }

    public BigDecimal getExchangeRates() {
        return exchangeRates;
    }
    public void setExchangeRates(BigDecimal exchangeRates) {
        this.exchangeRates = exchangeRates;
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }
    public void setSettlementDate(LocalDate settlementDate) {
        this.settlementDate = settlementDate;
    }

}
