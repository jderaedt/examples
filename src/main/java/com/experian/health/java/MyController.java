package com.experian.health.java;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.File;

public class MyController {

    @FXML
    private Button myButton;
    private State state;
//    private StateUtils stateValidator;
    public final String STATE_FILENAME = "StateList.csv";

    @FXML
    private TextField stateEntryArea;

    @FXML
    private TextArea outputTextArea;

    @FXML
    private void initialize() {
        File file = new File(this.getClass().getResource(STATE_FILENAME).getFile());
//        stateValidator = new StateUtils(file.getAbsolutePath());
        myButton.setOnAction((event) -> {
            outputTextArea.clear();
//            state = stateValidator.getStateByStateAbbreviation(stateEntryArea.getText());
            if (state != null) {
                outputTextArea.appendText(String.format("%s is the accepted abbreviation for %s.\n", state.getStateAbbreviation(), state.getStateName()));
            } else {
                outputTextArea.appendText("Invalid State Abbreviation!\n");
            }
        });
    }
}