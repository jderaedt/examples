package com.experian.health.java;

import java.time.LocalDate;
import java.time.Period;

public class Person {

    @Override
    public String toString() {
        return "com.experian.health.java.Person{" +
                "name='" + name + '\'' +
                ", dob=" + dob +
                ", nickname=" + nickname +
                '}';
    }

    private String name;
    private LocalDate dob;
	private String nickname;
    private String address;
    private String email;

    public Person( String name, LocalDate dob ) {
        this.name = name;
        this.dob = dob;
    }

    public Period getAge() {
        return dob.until(LocalDate.now());
    }

    public String getName() {
        return name;
    }

	public String getNickName() {
		return nickname;
	}
	

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String doCuss() {

        return "Shut the front door!";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
