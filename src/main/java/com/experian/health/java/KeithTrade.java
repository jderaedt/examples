package com.experian.health.java;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

/**
 * Created by keith.wenzler on 4/13/2016.
 */
public class KeithTrade {

    private Integer id;
    private Date tradeDate;
    private String buySell;
    private String currency1;
    private BigDecimal amount1;
    private Double exchangeRate;
    private String currency2;
    private BigDecimal amount2;
    private Date settlementDate;

    @Override
    public String toString() {

        return this.id + "," +
                new SimpleDateFormat("dd/MM/yyyy").format(this.tradeDate) + "," +
                this.buySell + "," +
                this.currency1 + "," +
                this.amount1.toString() + "," +
                this.exchangeRate.toString() + "," +
                this.currency2 + "," +
                this.amount2 + "," +
                new SimpleDateFormat("dd/MM/yyyy").format(this.settlementDate);
    }

//    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
//    public String newToString() {
//
//        StringBuilder sb = new StringBuilder();
//
//        sb.append(DATE_FORMAT.format(tradeDate))
//                .append(",")
//                .append(buySell)
//                .append(",");
//
//        return sb.toString();
//    }

    public KeithTrade( String trade ) {

        String[] tradeElements = trade.split(",");

        this.id = Integer.parseInt(tradeElements[0]);

        try {
            this.tradeDate = new SimpleDateFormat("dd/MM/yyyy").parse(tradeElements[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.buySell = tradeElements[2];

        this.currency1 = tradeElements[3];

        this.amount1 = new BigDecimal(tradeElements[4].replaceAll(",", ""));

        this.exchangeRate = Double.parseDouble(tradeElements[5]);

        this.currency2 = tradeElements[6];

        this.amount2 = new BigDecimal(tradeElements[7].replaceAll(",", ""));

        try {
            this.settlementDate = new SimpleDateFormat("dd/MM/yyyy").parse(tradeElements[8]);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

}
