package com.experian.health.java;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class JavaFxDemo extends Application {

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Event Handling");

        try {
            FXMLLoader loader = new FXMLLoader(JavaFxDemo.class.getResource("EventHandling.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Scene scene = new Scene(page);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
