package com.experian.health.java;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JeffTrade {

    private int id;
    private Date tradeDate;
    private String tradeAction;
    private String sourceCurrency;
    private BigDecimal sourceAmount;
    private double exchangeRate;
    private String targetCurrency;
    private BigDecimal targetAmount;
    private Date settlementDate;

    public static JeffTrade parseCsvLine(String line) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String[] elements = line.split(",");

        JeffTrade trade = new JeffTrade();
        trade.setId(Integer.parseInt(elements[0]));
        trade.setTradeDate(simpleDateFormat.parse(elements[1]));
        trade.setTradeAction(elements[2]);
        trade.setSourceCurrency(elements[3]);
        trade.setSourceAmount(new BigDecimal(elements[4]));
        trade.setExchangeRate(Double.parseDouble(elements[5]));
        trade.setTargetCurrency(elements[6]);
        trade.setTargetAmount(new BigDecimal(elements[7]));
        trade.setSettlementDate(simpleDateFormat.parse(elements[8]));
        return trade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTradeDate() {
        return new Date(tradeDate.getTime());
    }

    public void setTradeDate(Date tradeDate) {
        this.tradeDate = new Date(tradeDate.getTime());
    }

    public String getTradeAction() {
        return tradeAction;
    }

    public void setTradeAction(String tradeAction) {
        this.tradeAction = tradeAction;
    }

    public String getSourceCurrency() {
        return sourceCurrency;
    }

    public void setSourceCurrency(String sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    public BigDecimal getSourceAmount() {
        return sourceAmount;
    }

    public void setSourceAmount(BigDecimal sourceAmount) {
        this.sourceAmount = sourceAmount;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    public BigDecimal getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(BigDecimal targetAmount) {
        this.targetAmount = targetAmount;
    }

    public Date getSettlementDate() {
        return new Date(settlementDate.getTime());
    }

    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = new Date(settlementDate.getTime());
    }
}
