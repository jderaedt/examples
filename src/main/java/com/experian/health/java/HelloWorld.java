package com.experian.health.java;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Hello World example
 */
public class HelloWorld extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello Worlds!");
        Button btn = new Button();
        btn.setText("Say 'Hello Worlds'");
        btn.setOnAction(event -> System.out.println("Hello Worlds!"));
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        primaryStage.setScene(new Scene(root, 400, 325));
        primaryStage.show();
    }
}