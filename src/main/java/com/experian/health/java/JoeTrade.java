package com.experian.health.java;

import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * Created by joe.khym on 4/13/2016.
 */
public class JoeTrade {
    private Long id;
    private LocalDate tradeDate;
    private boolean isBuy;
    private boolean isSell;
    private String originalCurrencyType;
    private BigDecimal originalAmount;
    private Double originalExchangeRate;
    private String targetCurrencyType;
    private BigDecimal targetAmount;
    private LocalDate settleDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(LocalDate tradeDate) {
        this.tradeDate = tradeDate;
    }

    public boolean isBuy() {
        return isBuy;
    }

    public void setBuy(boolean buy) {
        isBuy = buy;
    }

    public boolean isSell() {
        return isSell;
    }

    public void setSell(boolean sell) {
        isSell = sell;
    }

    public String getOriginalCurrencyType() {
        return originalCurrencyType;
    }

    public void setOriginalCurrencyType(String originalCurrencyType) {
        this.originalCurrencyType = originalCurrencyType;
    }

    public BigDecimal getOriginalCurrencyAmount() {
        return originalAmount;
    }

    public void setOriginalCurrencyAmount(BigDecimal originalCurrencyAmount) {
        this.originalAmount = originalCurrencyAmount;
    }

    public Double getOriginalExchangeRate() {
        return originalExchangeRate;
    }

    public void setOriginalExchangeRate(Double originalExchangeRate) {
        this.originalExchangeRate = originalExchangeRate;
    }

    public String getTargetCurrencyType() {
        return targetCurrencyType;
    }

    public void setTargetCurrencyType(String targetCurrencyType) {
        this.targetCurrencyType = targetCurrencyType;
    }

    public BigDecimal getTargetAmount() {
        return targetAmount;
    }

    public void setTargetAmount(BigDecimal targetAmount) {
        this.targetAmount = targetAmount;
    }

    public LocalDate getSettleDate() {
        return settleDate;
    }

    public void setSettleDate(LocalDate settleDate) {
        this.settleDate = settleDate;
    }

    public void loadByParse(String csvLine) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String[] fields = csvLine.split(",");
        this.id = Long.parseLong(fields[0].trim());
        this.tradeDate = LocalDate.parse(fields[1].trim(), dtf);
        this.isBuy = fields[2].trim().toLowerCase().equals("buy");
        this.isSell = fields[2].trim().toLowerCase().equals("sell");
        this.originalCurrencyType = fields[3].trim().toUpperCase();
        this.originalAmount = stringToBigDecimal(fields[4].trim(), Locale.US);
        this.originalExchangeRate = Double.parseDouble(fields[5].trim());
        this.targetCurrencyType = fields[6].trim().toUpperCase();
        this.targetAmount = stringToBigDecimal(fields[7].trim().toUpperCase(), Locale.US);
        this.settleDate = LocalDate.parse(fields[8].trim(), dtf);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JoeTrade joeTrade = (JoeTrade) o;

        if (isBuy != joeTrade.isBuy) return false;
        if (isSell != joeTrade.isSell) return false;
        if (!id.equals(joeTrade.id)) return false;
        if (!tradeDate.equals(joeTrade.tradeDate)) return false;
        if (!originalCurrencyType.equals(joeTrade.originalCurrencyType)) return false;
        if (!originalAmount.equals(joeTrade.originalAmount)) return false;
        if (!originalExchangeRate.equals(joeTrade.originalExchangeRate)) return false;
        if (!targetCurrencyType.equals(joeTrade.targetCurrencyType)) return false;
        if (!targetAmount.equals(joeTrade.targetAmount)) return false;
        return settleDate.equals(joeTrade.settleDate);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + tradeDate.hashCode();
        result = 31 * result + (isBuy ? 1 : 0);
        result = 31 * result + (isSell ? 1 : 0);
        result = 31 * result + originalCurrencyType.hashCode();
        result = 31 * result + originalAmount.hashCode();
        result = 31 * result + originalExchangeRate.hashCode();
        result = 31 * result + targetCurrencyType.hashCode();
        result = 31 * result + targetAmount.hashCode();
        result = 31 * result + settleDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "JoeTrade{" +
                "id=" + id +
                ", tradeDate=" + tradeDate +
                ", isBuy=" + isBuy +
                ", isSell=" + isSell +
                ", originalCurrencyType='" + originalCurrencyType + '\'' +
                ", originalAmount=" + originalAmount +
                ", originalExchangeRate=" + originalExchangeRate +
                ", targetCurrencyType='" + targetCurrencyType + '\'' +
                ", targetAmount=" + targetAmount +
                ", settleDate=" + settleDate +
                '}';
    }

    //I would place this in a utils class but I was told to create one class...
    private static BigDecimal stringToBigDecimal(final String formattedString,
                                                 final Locale locale)
    {
        final DecimalFormatSymbols symbols;
        final char                 groupSeparatorChar;
        final String               groupSeparator;
        final char                 decimalSeparatorChar;
        final String               decimalSeparator;
        String                     fixedString;
        final BigDecimal           number;

        symbols              = new DecimalFormatSymbols(locale);
        groupSeparatorChar   = symbols.getGroupingSeparator();
        decimalSeparatorChar = symbols.getDecimalSeparator();

        if(groupSeparatorChar == '.')
        {
            groupSeparator = "\\" + groupSeparatorChar;
        }
        else
        {
            groupSeparator = Character.toString(groupSeparatorChar);
        }

        if(decimalSeparatorChar == '.')
        {
            decimalSeparator = "\\" + decimalSeparatorChar;
        }
        else
        {
            decimalSeparator = Character.toString(decimalSeparatorChar);
        }

        fixedString = formattedString.replaceAll(groupSeparator , "");
        fixedString = fixedString.replaceAll(decimalSeparator , ".");
        number      = new BigDecimal(fixedString);

        return (number);
    }
}
