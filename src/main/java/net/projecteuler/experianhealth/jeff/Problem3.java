package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

public class Problem3 implements Solution {
    private static final long NUMBER = 600851475143L;

    public static void main(String[] args) {
        Problem3 problem = new Problem3();
        System.out.println("Problem 3: " + problem.answer());
    }

    @Override
    public long answer() {
        long sqrt = (long) Math.sqrt(NUMBER);
        if (sqrt % 2 == 0)
            sqrt--;
        for (long i = sqrt; i > 0; i -= 2) {
            if (NUMBER % i == 0 && isPrime(i)) {
                return i;
            }
        }
        return 0;
    }

    private boolean isPrime(long n) {
        for (long i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
