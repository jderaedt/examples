package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

public class Problem7 implements Solution {
    public static void main(String[] args) {
        Problem7 problem = new Problem7();
        System.out.println("Problem 7: " + problem.answer());
    }

    @Override
    public long answer() {
        int primeCounter = 1;
        for (int i = 3; ; i += 2) {
            if (isPrime(i)) {
                primeCounter++;
                if (primeCounter == 10001) {
                    return i;
                }
            }
        }
    }

    private boolean isPrime(int n) {
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
