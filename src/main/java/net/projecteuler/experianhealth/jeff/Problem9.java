package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

public class Problem9 implements Solution {
    public static void main(String[] args) {
        Problem9 problem = new Problem9();
        System.out.println("Problem 9: " + problem.answer());
    }

    @Override
    public long answer() {
        for (long a = 1; ; a++) {
            for (long b = a + 1; a + b + b + 1 < 1000; b++) {
                for (long c = b + 1; a + b + c <= 1000; c++) {
                    if (a + b + c == 1000 && a * a + b * b == c * c) {
                        return a * b * c;
                    }
                }
            }
        }
    }
}
