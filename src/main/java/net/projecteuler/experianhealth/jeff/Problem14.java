package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

public class Problem14 implements Solution {
    public static void main(String[] args) {
        Problem14 problem = new Problem14();
        System.out.println("Problem 14: " + problem.answer());
    }

    @Override
    public long answer() {
        long max = 0;
        long number = 0;
        for (int i = 1; i < 1000000; i++) {
            long count = countCollatzChain(i);
            if (count > max) {
                max = count;
                number = i;
            }
        }
        return number;
    }

    private long countCollatzChain(long number) {
        long count = 0;
        long n = number;
        while (n != 1) {
            count++;
            if (n % 2 == 0) {
                n /= 2;
            } else {
                n = n * 3 + 1;
            }
        }
        return count + 1;
    }
}