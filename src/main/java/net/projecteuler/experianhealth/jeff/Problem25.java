package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

import java.math.BigInteger;

public class Problem25 implements Solution {
    public static void main(String[] args) {
        Problem25 problem = new Problem25();
        System.out.println("Problem 2: " + problem.answer());
    }

    @Override
    public long answer() {
        BigInteger last = BigInteger.valueOf(1);
        BigInteger current = BigInteger.valueOf(1);
        long index = 2;
        while (true) {
            index++;
            current = current.add(last);
            last = current.subtract(last);
            if (current.toString().length() >= 1000) {
                break;
            }
        }
        return index;
    }
}
