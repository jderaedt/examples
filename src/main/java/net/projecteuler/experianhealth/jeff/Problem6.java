package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

public class Problem6 implements Solution {
    public static void main(String[] args) {
        Problem6 problem = new Problem6();
        System.out.println("Problem 6: " + problem.answer());
    }

    @Override
    public long answer() {
        return getSquareOfSums() - getSumOfSquares();
    }

    private long getSumOfSquares() {
        long sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += Math.pow(i, 2);
        }
        return sum;
    }

    private long getSquareOfSums() {
        long sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += i;
        }
        return (long) Math.pow(sum, 2);
    }
}
