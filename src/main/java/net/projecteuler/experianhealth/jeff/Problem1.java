package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

import java.util.stream.IntStream;

public class Problem1 implements Solution {
    public static void main(String[] args) {
        Problem1 problem = new Problem1();
        System.out.println("Problem 1: " + problem.answer());
    }

    @Override
    public long answer() {
        try (IntStream range = IntStream.range(1, 1000)) {
            return range
                           .filter(i -> i % 3 == 0 || i % 5 == 0)
                           .sum();
        }
    }
}
