package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

public class Problem5 implements Solution {
    public static void main(String[] args) {
        Problem5 problem = new Problem5();
        System.out.println("Problem 5: " + problem.answer());
    }

    @Override
    public long answer() {
        for (int i = 20; ; i += 20) {
            boolean match = true;
            for (int j = 1; j <= 20; j++) {
                if (i % j != 0) {
                    match = false;
                    break;
                }
            }
            if (match) {
                return i;
            }
        }
    }
}
