package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

public class Problem2 implements Solution {
    public static void main(String[] args) {
        Problem2 problem = new Problem2();
        System.out.println("Problem 2: " + problem.answer());
    }

    @Override
    public long answer() {
        int last = 1;
        int current = 1;
        int sum = 0;
        while (current < 4000000) {
            current = current + last;
            last = current - last;
            if (current % 2 == 0) {
                sum += current;
            }
        }
        return sum;
    }
}
