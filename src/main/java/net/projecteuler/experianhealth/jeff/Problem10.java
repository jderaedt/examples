package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

public class Problem10 implements Solution {
    public static void main(String[] args) {
        Problem10 problem = new Problem10();
        System.out.println("Problem 10: " + problem.answer());
    }

    @Override
    public long answer() {
        long sum = 2;
        for (int i = 3; i < 2000000; i += 2) {
            if (isPrime(i)) {
                sum += i;
            }
        }
        return sum;
    }

    private boolean isPrime(int n) {
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
