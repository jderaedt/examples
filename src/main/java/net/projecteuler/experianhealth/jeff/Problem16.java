package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

import java.math.BigInteger;

public class Problem16 implements Solution {
    public static void main(String[] args) {
        Problem16 problem = new Problem16();
        System.out.println("Problem 16: " + problem.answer());
    }

    @Override
    public long answer() {
        long sum = 0;
        for (char n : BigInteger.valueOf(2).pow(1000).toString().toCharArray()) {
            sum += n - 48;
        }
        return sum;
    }
}
