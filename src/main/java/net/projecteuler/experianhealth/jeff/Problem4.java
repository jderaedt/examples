package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

public class Problem4 implements Solution {
    public static void main(String[] args) {
        Problem4 problem = new Problem4();
        System.out.println("Problem 4: " + problem.answer());
    }

    @Override
    public long answer() {
        int max = 0;
        for (int i = 100; i < 1000; i++) {
            for (int j = 100; j < 1000; j++) {
                int sum = i * j;
                if (sum > max && isPalindrome(sum)) {
                    max = sum;
                }
            }
        }
        return max;
    }

    private boolean isPalindrome(int number) {
        char[] numberArray = Integer.toString(number).toCharArray();
        int endIndex = numberArray.length - 1;
        int halfLength = numberArray.length / 2;

        for (int i = 0; i <= halfLength; i++) {
            if (numberArray[i] != numberArray[endIndex - i]) {
                return false;
            }
        }
        return true;
    }
}
