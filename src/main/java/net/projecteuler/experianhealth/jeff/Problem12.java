package net.projecteuler.experianhealth.jeff;

import net.projecteuler.experianhealth.Solution;

public class Problem12 implements Solution {
    public static void main(String[] args) {
        Problem12 problem = new Problem12();
        System.out.println("Problem 12: " + problem.answer());
    }

    @Override
    public long answer() {
        long sum = 0;
        for (long i = 1; ; i++) {
            sum += i;
            if (countDivisors(sum) > 500) {
                return sum;
            }
        }
    }

    private int countDivisors(long number) {
        if (number == 1) {
            return 1;
        }

        int count = 0;
        long limit = number;
        for (int i = 1; i < limit; i++) {
            if (number % i == 0) {
                count++;
                limit = number / i;
                if (limit != i) {
                    count++;
                }
            }
        }
        return count;
    }
}
