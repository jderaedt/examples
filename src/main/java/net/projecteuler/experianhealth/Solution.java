package net.projecteuler.experianhealth;

import java.io.UnsupportedEncodingException;

/**
 * Created by jdavies on 15/12/2015.
 */
public interface Solution {
    public long answer();
}
