package net.projecteuler.experianhealth.joe;

import net.projecteuler.experianhealth.Solution;

/**
 * Created by joe.khym on 4/13/2016.
 */
public class Problem5 implements Solution {
    public static void main(String[] args) {
        Problem5 problem5 = new Problem5();
        System.out.println("Problem 5: " + problem5.answer());
    }

    public long answer() {
//        2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
//        What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

        //Prime Numbers (1 to 20) = 2,3,5,7,11,13,17,19
        long result = 0;

        for (int i = 2520; i < 500_000_000L; i = i + 20) {
            if (i % 19 == 0) {
                if (i % 17 == 0) {
                    if (i % 13 == 0) {
                        if (i % 11 == 0) {
                            if (i % 7 == 0) {
                                if (i % 5 == 0) {
                                    if (i % 3 == 0) {
                                        if (i % 2 == 0) {
                                            if (i % 4 == 0 && i % 6 == 0 &&
                                                    i % 8 == 0 && i % 9 == 0 &&
                                                    i % 10 == 0 && i % 12 == 0 &&
                                                    i % 13 == 0 && i % 14 == 0 &&
                                                    i % 15 == 0 && i % 16 == 0 &&
                                                    i % 18 == 0 && i % 20 == 0) {
                                                result = i;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }
}