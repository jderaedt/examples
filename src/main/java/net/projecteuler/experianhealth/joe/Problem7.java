package net.projecteuler.experianhealth.joe;

import net.projecteuler.experianhealth.Solution;

/**
 * Created by joe.khym on 4/14/2016.
 */
public class Problem7 implements Solution {
    public static void main(String[] args) {
        Problem7 problem7 = new Problem7();
        System.out.println("Problem 7: " + problem7.answer());
    }
    public long answer() {
//        By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
//        What is the 10 001st prime number?
        long result = 0;
        int primeCounter = 2;
        for(int i = 3; i <=100_000_000L; i=i+2){
            if(isPrime(i))
            {
                result = i;
                //System.out.println(String.format("Prime Number %dth is %d", primeCounter, result));
                if(primeCounter == 10_001){
                    break;
                }
                primeCounter++;
            }
        }
        return result;
    }
    private static boolean isPrime(long number){
        boolean result = true;
        long squareRoot;
        if (number == 1 || number==2 || number ==3 || number == 5 || number == 7)
        {
            result = true;
        }
        else if (number % 2== 0)
        {
            result = false;
        }
        else{
            squareRoot = (long) Math.sqrt(number);
            for(int i=3; i<= squareRoot; i=i+2) {
                if (number % i == 0) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }
}