package net.projecteuler.experianhealth.joe;

import net.projecteuler.experianhealth.Solution;

import java.util.Objects;

/**
 * Created by joe.khym on 4/13/2016.
 */
public class Problem4 implements Solution {
    public static void main(String[] args) {
        Problem4 problem4 = new Problem4();
        System.out.println("Problem 4: " + problem4.answer());
    }
    public long answer() {
//        A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
//        Find the largest palindrome made from the product of two 3-digit numbers.
        final int STARTING_NUMBER = 999;

        long result = 0;
        long product;
        int startingNumber = STARTING_NUMBER;
        int stoppingNumber = startingNumber - 100;

        while (result==0)
        {
            for(int i = startingNumber; i > stoppingNumber; i--)
            {
                for(int j = startingNumber; j> stoppingNumber; j--)
                {
                    product = i*j;
                    if(isPalindrome(product) && product > result ){
                        result = product;
                    }
                }
            }
            startingNumber = stoppingNumber;
            stoppingNumber = startingNumber -100;
        }
        return result;
    }

    public static boolean isPalindrome(long number){
        boolean result = false;
        String stringNumber =  Objects.toString(number);
        String firstHalf = stringNumber.substring(0,stringNumber.length()/2);
        String secondHalf = stringNumber.substring((stringNumber.length()/2),stringNumber.length());
        String secondHalfReversed = "";
        for(int i = secondHalf.length()-1; i >= 0; i--)
        {
            secondHalfReversed += secondHalf.charAt(i);
        }
        return firstHalf.equals(secondHalfReversed);
    }
}