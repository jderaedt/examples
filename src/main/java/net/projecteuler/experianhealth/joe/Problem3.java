package net.projecteuler.experianhealth.joe;

import net.projecteuler.experianhealth.Solution;

/**
 * Created by joe.khym on 4/13/2016.
 */
public class Problem3 implements Solution {
    public static void main(String[] args) {
        Problem3 problem3 = new Problem3();
        System.out.println("Problem 3: " + problem3.answer());
    }

    public long answer(){
    //        The prime factors of 13195 are 5, 7, 13 and 29.
    //        What is the largest prime factor of the number 600851475143 ?
        long result = -1;
        int counter = 2;
        long number = 600851475143L;

        while(number != 1)
        {
            if(isPrime(number))
            {
                result = number;
                break;
            }
            while(number % counter == 0)
            {
                //System.out.println(String.format("Now working on %d / %d", number, counter));
                number /= counter;
            }
            counter++;
        }

        if (result==-1)
        {
            result = counter - 1;
        }
        return result;
    }

    private static boolean isPrime(long number){
        boolean result = true;
        long squareRoot;
        if (number == 1 || number==2 || number ==3 || number == 5 || number == 7)
        {
            result = true;
        }
        else if (number % 2== 0)
        {
            result = false;
        }
        else{
            squareRoot = (long) Math.sqrt(number);
            for(int i=3; i<= squareRoot; i=i+2) {
                if (number % i == 0) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }
}


//    public long answer() {
////        The prime factors of 13195 are 5, 7, 13 and 29.
////        What is the largest prime factor of the number 600851475143 ?
//
//        //Answer: Pollard's rho algorithm - https://en.wikipedia.org/wiki/Pollard%27s_rho_algorithm
//        final long NUMBER_TO_EXAMINE = 600851475142L;
//        long fixedX = 2;
//        int cycleSize = 2;
//        long x = 2;
//        long factor = 1;
//
//        while (factor == 1) {
//
//            for (int count=1;count <= cycleSize && factor <= 1;count++) {
//                x = (x*x+1)% NUMBER_TO_EXAMINE;
//                System.out.println(x-fixedX);
//                factor = greatestCommonDemoninator(x - fixedX, NUMBER_TO_EXAMINE);
//            }
//            cycleSize *= 2;
//            fixedX = x;
//        }
//        return factor;
//    }
//
//
//    private static long greatestCommonDemoninator (long x, long y)
//    {
//        long remainder;
//        while(y!=0){
//            remainder = x%y;
//            x=y;
//            y=remainder;
//        }
//        return x;
//    }
//}