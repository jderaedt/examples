package net.projecteuler.experianhealth.joe;

import net.projecteuler.experianhealth.Solution;

/**
 * Created by joe.khym on 4/14/2016.
 */
public class Problem6 implements Solution {
    public static void main(String[] args) {
        Problem6 problem6 = new Problem6();
        System.out.println("Problem 6: " + problem6.answer());
    }
    public long answer() {
//        The sum of the squares of the first ten natural numbers is,
//        12 + 22 + ... + 102 = 385
//        The square of the sum of the first ten natural numbers is,
//        (1 + 2 + ... + 10)2 = 552 = 3025
//        Hence the difference between the sum of the squares of the first ten natural numbers
//        and the square of the sum is 3025 − 385 = 2640.
//        Find the difference between the sum of the squares of the first one hundred
//        natural numbers and the square of the sum.
        long result = 0;

        long sumOfSquares = 0;
        long sum = 0;
        for(int i = 1; i <=100; i++){

            sum +=i;
            sumOfSquares += i*i;
        }
        result = (sum*sum) - sumOfSquares;
        return result;
    }
}