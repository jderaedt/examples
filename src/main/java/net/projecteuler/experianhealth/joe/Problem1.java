package net.projecteuler.experianhealth.joe;

import net.projecteuler.experianhealth.Solution;

/**
 * Created by joe.khym on 4/13/2016.
 */
public class Problem1 implements Solution {
    public static void main(String[] args) {
        Problem1 problem1 = new Problem1();
        System.out.println("Problem 1: " + problem1.answer());
    }
    public long answer() {
        //Find the sum of all the multiples of 3 or 5 below 1000.
        long result = 0;
        for (int i = 1; i < 1000; i++) {
            if ((i % 3 == 0) || (i % 5 == 0)) {
                result += i;
            }
        }
        return result;
    }
}
