package net.projecteuler.experianhealth.william;

import net.projecteuler.experianhealth.Solution;

import java.text.NumberFormat;
import java.util.Locale;

public class Problem6 implements Solution {
    public static void main(String[] args) {
        Problem6 problem6 = new Problem6();
        System.out.println("Problem 6: " + NumberFormat.getNumberInstance(Locale.US).format(problem6.answer()));
    }

    public long answer() {
        long sumSqaure = 0;
        long sumNatural = 0;

            for (int i = 1; i <= 100; i++) {

                sumSqaure = sumSqaure + (i * i);
                sumNatural += i;
            }

        return (sumNatural * sumNatural) - sumSqaure;
    }

}
