package net.projecteuler.experianhealth.william;

import net.projecteuler.experianhealth.Solution;

import java.text.NumberFormat;
import java.util.Locale;

public class Problem5 implements Solution {
    public static void main(String[] args) {
        Problem5 problem5 = new Problem5();
        System.out.println("Problem 5: " + NumberFormat.getNumberInstance(Locale.US).format(problem5.answer()));
    }

    public long answer() {
        long number = 1;
        long max = 20;

        boolean found = false;

        while(!found) {
            found = true;

            for (int i = 20; i > 0; i--) {

                if (number % i != 0)
                {
                    number++;
                    found = false;
                    break;
                }
            }
        }

        return number;
    }

}
