package net.projecteuler.experianhealth.william;

import net.projecteuler.experianhealth.Solution;

import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.Locale;

public class Problem3 implements Solution {
    public static void main(String[] args) {
        Problem3 problem3 = new Problem3();
        System.out.println("Problem 3: " + NumberFormat.getNumberInstance(Locale.US).format(problem3.answer()));
    }

    public long answer() {
        long sum = 0;
        long number = 600851475143l;
        long divider = 2l;

        while(number >= divider)
        {
            //Is the number divisible by the divider?
            if (number % divider != 0)
                divider++;
            else {
                sum = divider;
                number = number / divider;
            }
        }

        return sum;
    }
}
