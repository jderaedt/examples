package net.projecteuler.experianhealth.william;

import net.projecteuler.experianhealth.Solution;

public class Problem1 implements Solution {
    public static void main(String[] args) {
        Problem1 problem1 = new Problem1();
        System.out.println("Problem 1: " + problem1.answer());
    }

    public long answer() {

        int sum = 0;

        for (int i = 0; i < 1_000; i++) {
            if(i%3 == 0 || i%5 == 0)
                sum += i;
        }

        return sum;
    }
}
