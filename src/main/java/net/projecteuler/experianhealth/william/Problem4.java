package net.projecteuler.experianhealth.william;

import net.projecteuler.experianhealth.Solution;

import java.text.NumberFormat;
import java.util.Locale;

public class Problem4 implements Solution {
    public static void main(String[] args) {
        Problem4 problem4 = new Problem4();
        System.out.println("Problem 4: " + NumberFormat.getNumberInstance(Locale.US).format(problem4.answer()));
    }

    public long answer() {
        int n1 = 999;
        int n2 = 999;
        int highestNumber = 0;

        for (int i = n1; i > 1; i--) {
            for (int x = n2; x > 1; x--) {

                if(checkForPalidrone(i * x)) {
                    if((i * x) > highestNumber)
                        highestNumber = (i*x);
                }
            }
        }

        return highestNumber;
    }

    private boolean checkForPalidrone(int number)
    {
        String text = String.valueOf(number);

        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i) != text.charAt(text.length() - 1 - i))
                return false;
         }

        return true;
    }
}
