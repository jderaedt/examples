package net.projecteuler.experianhealth.william;

import net.projecteuler.experianhealth.Solution;

import java.text.NumberFormat;
import java.util.Locale;

public class Problem2 implements Solution {
    public static void main(String[] args) {
        Problem2 problem2 = new Problem2();
        System.out.println("Problem 2: " + NumberFormat.getNumberInstance(Locale.US).format(problem2.answer()));
    }

    public long answer() {
        int sum = 0;
        int n1 = 2;
        int n2 = 4;

        while((sum) < 4_000_000) {

            if((n1 + n2) % 2 == 0) {
                sum = n1 + n2;
                n1 = n2;
                n2 = sum;
            }
        }

        return sum;
    }
}
