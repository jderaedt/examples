package net.projecteuler.experianhealth.michael;

import net.projecteuler.experianhealth.Solution;

public class Problem6 implements Solution {
    public static void main(String[] args) {
        Problem6 Problem6 = new Problem6();

        System.out.println("Problem 6: " + Problem6.answer());
    }

    public long answer() {
        long result = 0;
        int sumOfSquares = 0;
        int squareOfSums = 0;
        int sumOfFirstHundred = 0;

        for (int i = 1; i <= 100; i++)
        {
            sumOfSquares += i * i;
            sumOfFirstHundred += i;
        }

        squareOfSums = sumOfFirstHundred * sumOfFirstHundred ;

        result = squareOfSums - sumOfSquares;


        return result;
    }
}
