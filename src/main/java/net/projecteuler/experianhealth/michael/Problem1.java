package net.projecteuler.experianhealth.michael;

import net.projecteuler.experianhealth.Solution;

public class Problem1 implements Solution {
    public static void main(String[] args) {
        Problem1 problem1 = new Problem1();

        System.out.println("Problem 1: " + problem1.answer());
    }

    public long answer() {
        long result = 0;
        for (int number = 3; number < 1000; number++) {
            if (number % 3 == 0 || number % 5 == 0)
            {
                result += number;
            }
        }
        return result;
    }
}
