package net.projecteuler.experianhealth.michael;

import net.projecteuler.experianhealth.Solution;

public class Problem2 implements Solution {
    public static void main(String[] args) {
        Problem2 Problem2 = new Problem2();

        System.out.println("Problem 2: " + Problem2.answer());
    }

    public long answer() {
        long result = 2;
        long fibonacciNumber = 2;
        long previousFibonacciNumber = 1;
        long currentFibonacciNumber = 2;

        while (fibonacciNumber < 4000000) {
            fibonacciNumber = previousFibonacciNumber + currentFibonacciNumber;
            previousFibonacciNumber = currentFibonacciNumber;
            currentFibonacciNumber = fibonacciNumber;

            if (fibonacciNumber % 2 == 0)
            {
                result += fibonacciNumber;
            }
        }

        return result;
    }
}
