package net.projecteuler.experianhealth.michael;

import net.projecteuler.experianhealth.Solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Problem4 implements Solution {
    public static void main(String[] args) {
        Problem4 Problem4 = new Problem4();

        System.out.println("Problem 4: " + Problem4.answer());
    }

    public long answer() {
        long result = 0;
        List<Integer> multipliers = new ArrayList<>();
        List<Integer> multiples = Collections.synchronizedList(new ArrayList<>());

        for (int overallCounter = 100; overallCounter > 10; overallCounter--) {
            for (int counter = (overallCounter * 10) - 1; counter >= (overallCounter* 9); counter--) {
                multipliers.add(counter);
            }

            multipliers.parallelStream().forEach(multiplier -> multiples.add(getMaxMultiple(multiplier, multipliers)));

            if (multiples.size() > 0) {
                Collections.sort(multiples);
                result = multiples.get(multiples.size() - 1);
                if (result > 0)
                {
                    break;
                }
            }
        }
        return result;
    }

    public boolean isPalindrome(String potentialPalindrome) {

        boolean validPalindrome = false;

        for (int index = 0; index < potentialPalindrome.length()/2; index++)
        {
            if (potentialPalindrome.charAt(index) == potentialPalindrome.charAt(potentialPalindrome.length() - 1 - index))
            {
                validPalindrome = true;
            }
            else
            {
                validPalindrome = false;
                break;
            }
        }

        return validPalindrome;
    }

    public int getMaxMultiple(int multiplier, List<Integer> multipliers)
    {
        int maxMultiple = 0;
        List<Integer> multiples = Collections.synchronizedList(new ArrayList<>());

        multipliers.parallelStream().forEach(multiplicand -> {
            int multiple = multiplicand * multiplier;
            if (isPalindrome(String.valueOf(multiple))) {
                multiples.add(multiple);
            }
        });

        if (multiples.size() > 0) {
            maxMultiple = Collections.max(multiples);
        }

        return maxMultiple;
    }
}
