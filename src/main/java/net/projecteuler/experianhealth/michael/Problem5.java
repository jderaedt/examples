package net.projecteuler.experianhealth.michael;

import net.projecteuler.experianhealth.Solution;

public class Problem5 implements Solution {
    public static void main(String[] args) {
        Problem5 Problem5 = new Problem5();

        System.out.println("Problem 5: " + Problem5.answer());
    }

    public long answer() {
        long result = 0;
        int startingPointAndCounter = 1;

        for (int counter = 2; counter <= 20; counter++)
        {
            if (isPrime(counter)) {
                startingPointAndCounter = counter * startingPointAndCounter;
            }
        }

        // 9699690 = all primes from 1-20 multiplied together
        for (int counter = startingPointAndCounter; counter < 1000000000; counter = counter + startingPointAndCounter)
        {
            // divisible by 20 is also divisible by 2,4,5,10
            // divisible by 18 is also divisible by 2,3,6,9
            // divisible by 16 is also divisible by 2,4,8
            // divisible by 15 is also divisible by 3,5
            // divisible by 14 is also divisible by 2,7
            // divisible by 12 is also divisible by 2,3,6
            if (counter % 19 == 0 &&
                counter % 18 == 0 &&
                counter % 17 == 0 &&
                counter % 16 == 0 &&
                counter % 15 == 0 &&
                counter % 14 == 0 &&
                counter % 13 == 0 &&
                counter % 12 == 0 &&
                counter % 11 == 0)
            {
                result = counter;
                break;
            }
        }
        return result;
    }

    public boolean isPrime(final long potentialPrime) {
        boolean prime = true;
        final long squareRootOfPotentialPrime = Math.round(Math.sqrt(potentialPrime));
        if (potentialPrime > 2 ) {
            if (potentialPrime % 2 == 0) {
                prime = false;
            }
            else {
                for (int counter = 3; counter <= squareRootOfPotentialPrime; counter = counter + 2) {
                    if (potentialPrime % counter == 0) {
                        prime = false;
                        break;
                    }
                }
            }
        }
        return prime;
    }
}
