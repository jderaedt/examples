package net.projecteuler.experianhealth.michael;

import net.projecteuler.experianhealth.Solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Problem3 implements Solution {
    public static void main(String[] args) {
        Problem3 Problem3 = new Problem3();

        System.out.println("Problem 3: " + Problem3.answer());
    }

    private final static long NUMBER = 600851475143L;
    private final static long SQUAREROOT_OF_NUMBER = Math.round(Math.sqrt(NUMBER));

    public long answer() {
        long result;

        result = getMaxPrimeFactor();

        return result;
    }

    public long getMaxPrimeFactor() {
        long result = 0;

        // rounded square root is even, make it odd
        for (long counter = SQUAREROOT_OF_NUMBER - 1; counter > 0; counter = counter - 2) {
            if (NUMBER % counter == 0 && isPrime(counter))
            {
                result = counter;
                break;
            }
        }

        return result;
    }


    public boolean isPrime(final long potentialPrime) {
        boolean prime = true;
        final long squareRootOfPotentialPrime = Math.round(Math.sqrt(potentialPrime));

        for (int counter = 3; counter <= squareRootOfPotentialPrime; counter = counter + 2) {
            if (potentialPrime % counter == 0)
            {
                prime = false;
                break;
            }
        }

        return prime;
    }
}
