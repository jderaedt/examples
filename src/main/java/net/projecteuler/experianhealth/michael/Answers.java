package net.projecteuler.experianhealth.michael;

import net.projecteuler.experianhealth.Solution;

public class Answers {

    public static void main(String[] args) {

        for (int problem = 1; problem < 600; problem++) {
            Solution solution = getSolutionForProblem(problem);
            if (solution != null) {
                long start = System.nanoTime();
                long answer = solution.answer();
                long duration = System.nanoTime() - start;
                if (duration < 10_000_000_000L) {
                    long tps = 10_000_000_000L / duration;
                    for (long count = 0; count < tps; count++) {
                        start = System.nanoTime();
                        answer = solution.answer();
                    }
                    duration = System.nanoTime() - start;
                }
                if (duration < 1_000)
                    System.out.printf("Problem %d:\t%d\t(%d nS)%n", problem, answer, duration);
                else if (duration < 1_000_000)
                    System.out.printf("Problem %d:\t%d\t(%,.3f µS)%n", problem, answer, duration / 1e3);
                else if (duration < 1_000_000_000)
                    System.out.printf("Problem %d:\t%d\t(%,.3f mS)%n", problem, answer, duration / 1e6);
                else
                    System.out.printf("Problem %d:\t%d\t(%,.3f seconds)%n", problem, answer, duration / 1e9);
            }
        }
    }

    public static Solution getSolutionForProblem(int prob) {
        Solution solution = null;

        try {
            Class<Solution> problem1 = (Class<Solution>) Class.forName("net.projecteuler.experianhealth.michael.Problem" + prob);
            solution = problem1.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            // do nothing, nothing valid found
        }
        return solution;
    }
}
