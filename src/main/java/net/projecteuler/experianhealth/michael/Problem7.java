package net.projecteuler.experianhealth.michael;

import net.projecteuler.experianhealth.Solution;

public class Problem7 implements Solution {
    public static void main(String[] args) {
        Problem7 Problem7 = new Problem7();

        System.out.println("Problem 7: " + Problem7.answer());
    }

    public long answer() {
        long result = 0;
        int primeNumberIndex = 1;
        int lastPrimeNumber = 2;

        while (primeNumberIndex < 10002)
        {
            while (!isPrime(lastPrimeNumber))
            {
                lastPrimeNumber ++;
            }

            primeNumberIndex++;
            lastPrimeNumber ++;
        }
        result = lastPrimeNumber - 1;
        return result;
    }
    public boolean isPrime(final long potentialPrime) {
        boolean prime = true;
        final long squareRootOfPotentialPrime = Math.round(Math.sqrt(potentialPrime));
        if (potentialPrime > 2 ) {
            if (potentialPrime % 2 == 0) {
                prime = false;
            }
            else {
                for (int counter = 3; counter <= squareRootOfPotentialPrime; counter = counter + 2) {
                    if (potentialPrime % counter == 0) {
                        prime = false;
                        break;
                    }
                }
            }
        }
        return prime;
    }
}
