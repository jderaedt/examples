package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

public class Problem15 implements Solution {
    public long answer() {
        int width = 20;
        int height = 20;

        long[][] grid = new long[height][width];

        for (int d = 0; d < height; d++) {
            for (int l = 0; l < width; l++) {
                if (d == 0)
                    grid[d][l] = l + 2;
                else if (l == 0)
                    grid[d][l] = d + 2;
                else
                    grid[d][l] = grid[d - 1][l] + grid[d][l - 1];
            }
        }
        return grid[height - 1][width - 1];
    }
}
