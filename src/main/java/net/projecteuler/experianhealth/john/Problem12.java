package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

import java.util.stream.IntStream;

public class Problem12 implements Solution {
    public long answer() {
        return IntStream.iterate(0, i -> i + 1).parallel()
                .map(x -> x * (x + 1) / 2)
                .filter(x -> countDivisors(x) > 500)
                .findFirst().getAsInt();
    }

    private static int countDivisors(long n) {
        int count = 0;
        for (long x = 1; x * x <= n; x++) {
            if (n % x == 0) {
                count++;
            }
        }
        return 2 * count - 1;
    }
}
