package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

import java.util.Comparator;
import java.util.stream.IntStream;

public class Problem14 implements Solution {
    public long answer() {
        Tuple tuple = IntStream.range(1, 1_000_000)
                .mapToObj(n -> countCollatzTerms(n))
                .max(Comparator.comparing(i -> i.terms))
                .get();
        return tuple.n;
    }

    private static class Tuple {
        protected long n;
        protected int terms;
    }

    private Tuple countCollatzTerms(long n) {
        int count = 1;
        Tuple tuple = new Tuple();
        tuple.n = n;
        do {
            n = n % 2 == 0 ? n / 2 : 3 * n + 1;
            count++;
        } while (n > 1);
        tuple.terms = count;
        return tuple;
    }
}
