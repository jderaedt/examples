package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

import java.util.ArrayList;
import java.util.List;

public class Problem10 implements Solution {
    public long answer() {
        List<Integer> primes = new ArrayList<>();

        primes.add(2);

        for (int i = 3; i < 2_000_000; i += 2) {
            boolean isPrime = true;
            for (int index = 0; primes.get(index) * primes.get(index) <= i; index++) {
                int prime = primes.get(index);
                if (i % prime == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                primes.add(i);
            }
        }
        return primes.stream().mapToLong(x -> x.longValue()).sum();
    }
}
