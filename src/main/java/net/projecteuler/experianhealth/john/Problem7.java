package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

public class Problem7 implements Solution {
    public long answer() {
        long[] primes = new long[10_001];
        int primeCount = 1;

        primes[0] = 2;

        for (int i = 3; primeCount < primes.length; i += 2) {
            boolean isPrime = true;
            for (int index = 0; index < primeCount && primes[index] * primes[index] <= i; index++) {
                if (i % primes[index] == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime)
                primes[primeCount++] = i;
        }
        return primes[10_000];
    }
}
