package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

import java.util.stream.IntStream;

public class Problem5 implements Solution {
    public long answer() {
        int[] one2Twenty = IntStream.rangeClosed(1, 20).toArray();
        return lcm(one2Twenty);
    }

    private static int gcd(int a, int b) {
        while (b > 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    private static int lcm(int a, int b) {
        return a * (b / gcd(a, b));
    }

    private static int lcm(int[] input) {
        int result = input[0];
        for (int i = 1; i < input.length; i++)
            result = lcm(result, input[i]);
        return result;
    }
}
