package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;
import java.util.stream.IntStream;

public class Problem1 implements Solution {
    public long answer() {
        return IntStream.range(2, 1000)
                .filter(n -> n % 3 == 0 || n % 5 == 0)
                .sum();
    }
}
