package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

public class Problem4 implements Solution {
    public long answer() {
        int num1;
        int num2 = 0;

        loops:
        for (num1 = 999; num1 > 900; num1--) {
            for (num2 = num1; num2 > 900; num2--) {
                if (isPalindromic(num1 * num2)) {
                    break loops;
                }
            }
        }
        return num1 * num2;
    }

    private boolean isPalindromic(int num) {
        String string = Integer.toString(num);
        int length = string.length();
        for (int i = 0; i < length; i++) {
            if (string.charAt(i) != string.charAt(length - 1 - i))
                return false;
        }
        return true;
    }
}
