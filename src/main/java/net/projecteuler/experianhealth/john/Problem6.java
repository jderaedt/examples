package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

import java.util.stream.IntStream;

public class Problem6 implements Solution {
    public long answer() {
        int max = 100;
        int sumOfSquares = IntStream.rangeClosed(1, max)
                .map(x -> x * x)
                .sum();
        int sumOfNumbers = max * (max + 1) / 2;
        return sumOfNumbers * sumOfNumbers - sumOfSquares;
    }
}
