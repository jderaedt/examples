package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

public class Problem9 implements Solution {
    public long answer() {
        for (int a = 1; a < 1_000; a++) {
            for (int b = 1; b < 1_000; b++) {
                int c = (int) Math.sqrt(a * a + b * b);
                if (a * a + b * b == c * c && a + b + c == 1_000) {
                    return a * b * c;
                }
            }
        }
        return 0;   // This should never be reached
    }
}
