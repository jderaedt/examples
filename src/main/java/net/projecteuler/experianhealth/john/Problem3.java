package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

public class Problem3 implements Solution {
    public long answer() {
        long start = 600851475143L;
        long number = start;

        for (int i = 2; i * i <= number; i += 2) {
            while (number % i == 0) {
                number /= i;
            }
            if (i == 2) i = 1; // Becomes 3 on the next loop
        }
        return number;
    }
}
