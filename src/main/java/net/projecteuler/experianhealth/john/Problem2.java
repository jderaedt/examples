package net.projecteuler.experianhealth.john;

import net.projecteuler.experianhealth.Solution;

public class Problem2 implements Solution {
    public long answer() {
        int total = 0;
        int previous = 0;
        int latest = 1;
        int fib;

        while ((latest + previous) < 4_000_000) {
            fib = previous + latest;
            previous = latest;
            latest = fib;
            if (fib % 2 == 0)
                total += fib;
        }
        return total;
    }
}
